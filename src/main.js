import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// components
import Navbar from '@/components/layout/Navbar'
import Footer from '@/components/layout/Footer'
import sidebar from '@/components/layout/menu/sidebar'
import burger from '@/components/layout/menu/burger'

Vue.component('navbar', Navbar)
Vue.component('Footer', Footer)
Vue.component('sidebar', sidebar)
Vue.component('burger', burger)

import { library } from '@fortawesome/fontawesome-svg-core'
import {
    faHotel,
    faCalendarAlt,
    faUser,
    faBell,
    faSearch,
    faChevronLeft,
    faChevronRight,
    faBars,
    faEnvelope,
    faPhone,
    faMapMarkerAlt,
    faLocationArrow,
    faPhoneSquareAlt,
    faUsers,
    faBuilding,
    faPlus,
    faCalendar,
    faClock,
    faTimes,
    faSpinner,
    faTrashAlt,
    faLock,
    faLockOpen,
    faKey,
    faSignal,
    faSignInAlt,
    faPlug,
    faCogs,
    faProjectDiagram,
    faChartBar,
    faUnlockAlt,
    faGift,
    faGlobe,
    faAngleLeft,
    faAngleRight,
    faAngleDoubleLeft,
    faAngleDoubleRight,
    faTrash,
    faDotCircle,
    faAngleDoubleDown,
    faTh,
    faHeart,
    faEye,
    faStar,
    faAngleDown,
    faMap,
    faMapMarker
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.config.productionTip = false

library.add(faHotel, faMapMarker, faCalendarAlt, faUser, faSearch, faEnvelope, faPhoneSquareAlt, faBell, faChevronLeft, faChevronRight,
    faBars, faPhone, faMapMarkerAlt, faLocationArrow,
    faUsers, faBuilding, faPlus, faCalendar,
    faClock, faTimes, faSpinner, faTrashAlt, faKey,
    faLock, faLockOpen, faSignal, faSignInAlt, faPlug,
    faCogs, faProjectDiagram, faChartBar, faUnlockAlt,
    faGift, faGlobe, faAngleLeft, faAngleRight,
    faAngleDoubleLeft, faAngleDoubleRight, faTrash,
    faDotCircle, faAngleDoubleDown, faTh, faHeart,
    faEye, faStar, faAngleDown)

Vue.component('font-awesome-icon', FontAwesomeIcon)
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

import Toasted from 'vue-toasted';
Vue.use(Toasted)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')