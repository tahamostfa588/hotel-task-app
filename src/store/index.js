import Vue from 'vue'
import Vuex from 'vuex'

import { auth } from './modules/auth'
import { alert } from './modules/alert';
import { menu } from './modules/menu';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        auth,
        alert,
        menu
    }
})