import { authService } from '../../services';
import router from '../../router';

const user = JSON.parse(localStorage.getItem('user'));
const users = JSON.parse(localStorage.getItem('users'));

const state = user ? { status: { loggedIn: true }, user } : { status: { loggedIn: false }, user: null };

const actions = {
    login({ dispatch, commit }, user) {
        commit('requesting', user);
        let users = JSON.parse(localStorage.getItem('users')) ? JSON.parse(localStorage.getItem('users')) : []
        console.log(users);

        if (users.some(U => U.email === user.email && U.password === user.password)) {
            let userIndex = users.findIndex(x => x.email === user.email);
            let loggedInUser = users[userIndex]
            localStorage.setItem('user', JSON.stringify(loggedInUser));
            dispatch('alert/success', 'Login successful', { root: true });
            commit('loginSuccess', loggedInUser);
            router.push('/');
        } else {
            let error = 'There is an error !!'
            commit('loginFailure');
            dispatch('alert/error', error, { root: true });
        }
    },
    logout({ commit }) {
        localStorage.removeItem('user');
        commit('logout');
    },
    register({ dispatch, commit }, user) {
        commit('requesting', user);
        let users = JSON.parse(localStorage.getItem('users')) ? JSON.parse(localStorage.getItem('users')) : []
        if (users.some(U => U.email === user.email)) {
            let error = 'Your email is already exist, try to login'
            commit('loginFailure');
            dispatch('alert/error', error, { root: true });
        } else {
            users.push(user);
            localStorage.setItem('users', JSON.stringify(users));
            localStorage.setItem('user', JSON.stringify(user));
            dispatch('alert/success', 'Registration successful', { root: true });

            commit('loginSuccess', error);
            router.push('/');
        }
    }
};

const mutations = {
    loginRequest(state, user) {
        state.status = { loggingIn: true };
        state.user = user;
    },
    loginSuccess(state, user) {
        state.status = { loggedIn: true };
        state.user = user;
    },
    loginFailure(state) {
        state.status = { loggedIn: false };
        state.user = null;
    },
    logout(state) {
        state.status = { loggedIn: false };
        state.user = null;
    },
    requesting(state, user) {
        state.status = { requesting: true };
    },
    registerSuccess(state, user) {
        state.status = {};
    },
    registerFailure(state, error) {
        state.status = {};
    }
};

export const auth = {
    namespaced: true,
    state,
    actions,
    mutations
};