const state = {
    isMenuOpen: false
}

const getters = {
    getIsMenuOpen(state) {
        return state.isMenuOpen
    }
}

const mutations = {
    setMenuStatus(state) {
        state.isMenuOpen = !state.isMenuOpen
    },
}

const actions = {
    toggle(context) {
        context.commit('setMenuStatus')
    },
}

export const menu = {
    namespaced: true,
    state,
    actions,
    mutations,
    getters
};